#[1] import module yang anda buat

import pet

#Fungsi main menguji module
def main():

    #[2] Minta pengguna menginput nama, jenis dan umur
    nama = input('Siapa nama hewan peliharaan Anda? ')
    jenis = input('Jenis hewan apa peliharaan Anda? ')
    umur = int(input('Berapa umur hewan peliharaan Anda? '))
    

    #[3] Buat objek hewan
    hewan = pet.Pet(nama, jenis, umur)
    
    #[4] Tampilkan nama,jenis dan umur
    print()
    print('Nama:', hewan.get_nama())
    print('Jenis hewan:', hewan.get_jenis())
    print('Umur (tahun):', hewan.get_umur())
    

#Panggil fungsi main
main()


# DIBAWAH INI ADALAH KODE PENGUJIAN JANGAN DI COPAS

# kitty = pet.Pet('Kitty', 'Kucing', '3')
# print('Nama:', kitty.get_nama())
# print('Jenis:', kitty.get_jenis())
# print('Umur (tahun):', kitty.get_umur())
# print()
# print('Set nama, jenis, dan umur berbeda.')
# kitty.set_nama('Zara')
# kitty.set_jenis('Kucing Persia')
# kitty.set_umur('4')
# print('Nama:', kitty.get_nama())
# print('Jenis:', kitty.get_jenis())
# print('Umur (tahun):', kitty.get_umur())
