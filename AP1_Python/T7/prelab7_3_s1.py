# Class Orang mempunyai method-method berikut:
# __init__() yang menerima argumen nama dan umur dan menginisialisasi attribute __nama dan __umur dengan argumen-argumen tersebut.
# tampilkan_nama() yang menampilkan nama dari Orang tersebut.
# tampilkan_umur() yang menampilkan umur Orang tersebut.

class Orang:
    def __init__(self, nama, umur):
        self.__nama = nama
        self.__umur = umur
    
    def tampilkan_nama(self):
        print('Nama saya adalah',self.__nama)
    
    def tampilkan_umur(self):
        print('Umur saya adalah',self.__umur)

# Class Siswa merupakan turunan dari class Orang. Class Siswa mempunyai method-method berikut:

class Siswa(Orang):
    def __init__(self, nama, umur, tingkat):
        super().__init__(nama, umur)
        self.__tingkat = tingkat
    
    def tampilkan_tingkat(self):
        print('Saya tingkat',self.__tingkat)

# Class Pekerja merupakan turunan dari class Orang. Class Pekerja mempunyai method-method berikut:

class Pekerja(Orang):
    def __init__(self, nama, umur, pekerjaan):
        super().__init__(nama, umur)
        self.__pekerjaan = pekerjaan
    
    def tampilkan_pekerjaan(self):
        print('Pekerjaan saya',self.__pekerjaan)
