# Tuliskan definisi class-class yang diminta di bawah
class Burung:
    def __init__(self, spesies: str) -> None:
        self.__spesies = spesies
        
    def tampilkan_spesies(self):
        print(f"Saya adalah seekor {self.__spesies}")
        
    @staticmethod
    def buat_suara():
        print(f"Ciut... Ciut...")
        
class Ayam(Burung):
    def __init__(self) -> None:
        Burung.__init__(self, "ayam")
        
    @staticmethod
    def buat_suara():
        print(f"Kukuruyuuuk...")
        
class Bebek(Burung):
    def __init__(self) -> None:
        Burung.__init__(self, "bebek")
        
    @staticmethod
    def buat_suara():
        print(f"Kwek! Kwek!")
