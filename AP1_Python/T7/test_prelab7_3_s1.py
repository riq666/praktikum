import prelab7_3_s1 as test

orang1 = test.Orang('Budi Susilo', 33)
siswa1 = test.Siswa('Andi Geledek', 17, '12')
pekerja1 = test.Pekerja('Tomi Gunawan', 32, 'Wiraswasta')

print('Informasi orang 1:')
orang1.tampilkan_nama()
orang1.tampilkan_umur()
print()

print('Informasi siswa 1:')
siswa1.tampilkan_nama()
siswa1.tampilkan_umur()
siswa1.tampilkan_tingkat()
print()

print('Informasi pekerja 1:')
pekerja1.tampilkan_nama()
pekerja1.tampilkan_umur()
pekerja1.tampilkan_pekerjaan()