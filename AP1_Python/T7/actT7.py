# [1] Import module yang telah anda buat
import mahasiswa as gundar

# Fungsi main untuk menguji class Mahasiswa
def main():
    
    # [2] Buat tiga object mahasiswa dari class Mahasiswa sesuai dengan data pada tabel
    mahasiswa1 = gundar.Mahasiswa("Budi Susilo", 11223344, "Teknik Informatika", "0811-1234-7896")
    mahasiswa2 = gundar.Mahasiswa("Inggrid Wijaya", 11286755, "Sistem Informasi", "0819-0086-9978")
    mahasiswa3 = gundar.Mahasiswa("Amelia", 11245645, "Ilmu Komunikasi", "0838-7765-0987")
    
    # [3] Tampilkan Data Mahasiswa 1
    mahasiswa1.show()

    # [4] Tampilkan Data Mahasiswa 2
    mahasiswa2.show()    

    # [5] Tampilkan Data Mahasiswa 3
    mahasiswa3.show()
    
    
# Panggil fungsi main
main()
